import Vue from "vue";
import Vuex from "vuex";
// import createPersistedState from "vuex-persistedstate";

import app from "./modules/app";
import auth from "./modules/auth";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production"; //eslint-disable-line no-undef

console.log(debug);

export default new Vuex.Store({
  modules: {
    app,
    auth
  },
  strict: debug
  // plugins: [createPersistedState()]
});
