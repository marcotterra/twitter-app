import Vue from "vue";
import app from "@/configs/app";
import ls from "@/services/localstorage";

const state = {
  user: {},
  token: ls.getToken(app.jwtKey)
};

const mutations = {
  SET_USER(state, user) {
    state.user = user;
  },

  SET_TOKEN(state, token) {
    state.token = token;
    ls.setToken(app.jwtKey, state.token);
  },

  SET_LOGOUT(state) {
    state.token = null;
    ls.removeToken(app.jwtKey);
  }
};

const actions = {
  async login({ commit, dispatch }, credentials) {
    await Vue.axios
      .post("/auth/login", credentials)
      .then(response => {
        commit("SET_TOKEN", response.data.token);
      })
      .then(() => {
        dispatch("checkUser");
      })
      .catch(error => {
        alert(`Erro: \n${error.response.data.message}`);
      });
  },

  register({ commit }, credentials) {
    Vue.axios
      .post("/auth/register", credentials)
      .then(() => {
        alert("Registrado com sucesso");
      })
      .catch(error => {
        alert(`Erro: ${error.response.data.message}`);
      });
  },

  async checkUser({ commit, dispatch }) {
    await Vue.axios
      .get("/users/me")
      .then(response => {
        commit("SET_USER", response.data.user);
      })
      .catch(error => {
        console.log(`Erro: ${error.response.data.message}`);
        dispatch("logout");
      });
  },

  logout({ commit }) {
    commit("SET_LOGOUT");
  }
};

const getters = {
  token: state => state.token,

  currentUser: state => state.user,

  isAuth: state => !!state.token
};

export default {
  state,
  actions,
  mutations,
  getters
};
