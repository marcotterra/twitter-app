import Vue from "vue";
import types from "@/configs/types";

const state = {
  title: "",
  timeline: {},
  timelineType: "t",
  loading: false
};

const mutations = {
  [types.SET_TITLE](state, title) {
    state.title = title;
  },

  [types.SET_TWEET](state, val) {
    state.timeline.tweets.push(val);
  },

  [types.SET_TIMELINE_TWEETS](state, val) {
    state.timeline.tweets = val;
  },

  [types.SET_TIMELINE_USERS](state, val) {
    state.timeline.users = val;
  },

  [types.CHANGE_TIMELINE_TYPE](state, type) {
    state.timelineType = type;
  },

  SET_LOADING(state, val) {
    state.loading = val;
  }
};

const actions = {
  setTitle({ commit }, title) {
    commit(types.SET_TITLE, title);
  },

  newTweet({ commit }, tweet) {
    Vue.axios
      .post("/tweets", tweet)
      .then(response => {
        commit(types.SET_TWEET, response.data);
      })
      .catch(error => {
        alert(error.response.data.message);
      });
  },

  setTimeline({ commit }) {
    commit("SET_LOADING", true);
    Vue.axios
      .get("/tweets")
      .then(response => {
        commit(types.SET_TIMELINE_USERS, null);
        commit(types.CHANGE_TIMELINE_TYPE, "t");
        commit(types.SET_TIMELINE_TWEETS, response.data);
      })
      .catch(error => {
        alert(error.response.data.message);
      })
      .finally(() => {
        commit("SET_LOADING", false);
      });
  },

  findUser({ commit }, value) {
    commit("SET_LOADING", true);
    Vue.axios
      .get(`/users/search/${value}`)
      .then(response => {
        commit(types.SET_TIMELINE_TWEETS, null);
        commit(types.CHANGE_TIMELINE_TYPE, "u");
        commit(types.SET_TIMELINE_USERS, response.data);
      })
      .catch(error => {
        alert(error.response.data.message);
      })
      .finally(() => {
        commit("SET_LOADING", false);
      });
  }
};

const getters = {
  pageTitle: state => state.title,

  getTimeline: state => state.timeline,

  getTimelineType: state => state.timelineType,

  isLoading: state => state.loading
};

export default { state, mutations, actions, getters };
