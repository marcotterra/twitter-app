const types = {
  /**
   * setUser
   */
  SET_AUTH: "setUser",

  /**
   * purgeAuth
   */
  PURGE_AUTH: "purgeAuth",

  /**
   * setAlert
   */
  SET_ALERT: "setAlert",

  /**
   * purgeAlert
   */
  PURGE_ALERT: "purgeAlert",

  /**
   * setTitle
   */
  SET_TITLE: "setTitle",

  /**
   * setAppName
   */
  SET_APPNAME: "setAppName",

  /**
   * changeDrawer
   */
  CHANGE_DRAWER: "changeDrawer",

  SET_TWEET: "setTweet",

  SET_TIMELINE_TWEETS: "setTimeline",

  SET_TIMELINE_USERS: "setTimelineUsers",

  SET_TIMELINE_TYPE_T: "setTimeLineT",

  SET_TIMELINE_TYPE_U: "setTimeLineU",

  CHANGE_TIMELINE_TYPE: "changeTimelineT"
};

export default types;
