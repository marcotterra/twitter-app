import Vue from "vue";
import {
  Vuetify,
  VApp,
  VBtn,
  VCard,
  VDivider,
  VForm,
  VGrid,
  VIcon,
  VList,
  VNavigationDrawer,
  VSubheader,
  VTextField,
  VToolbar,
  VTooltip,
  VAvatar,
  VDialog,
  VTextarea,
  VProgressCircular
} from "vuetify";
import "vuetify/src/stylus/app.styl";

Vue.use(Vuetify, {
  theme: {
    primary: "#ee44aa",
    secondary: "#424242",
    accent: "#82B1FF",
    error: "#FF5252",
    info: "#2196F3",
    success: "#4CAF50",
    warning: "#FFC107"
  },
  components: {
    VApp,
    VBtn,
    VCard,
    VDivider,
    VForm,
    VGrid,
    VIcon,
    VList,
    VNavigationDrawer,
    VSubheader,
    VTextField,
    VToolbar,
    VTooltip,
    VAvatar,
    VDialog,
    VTextarea,
    VProgressCircular
  }
});
