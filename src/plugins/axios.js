import Vue from "vue";
import axios from "axios";
import app from "@/configs/app";
import ls from "@/services/localstorage";

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || '';
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

let config = {
  baseURL: app.apiUrl
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
};

const _axios = axios.create(config);

_axios.interceptors.request.use(
  function(config) {
    config.headers.Authorization = `Bearer ${ls.getToken(app.jwtKey)}`;
    return config;
  },
  function(error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
_axios.interceptors.response.use(
  function(response) {
    // Do something with response data
    const token = response.headers["Authorization"] || response.data["token"];

    token && ls.setToken(app.jwtKey, token);

    return response;
  },
  function(error) {
    // Do something with response error

    if (error.response.status === 400 || error.response.status === 401) {
      // Caso a requisição não for uma tentativa de login
      if (!(error.config.method === "post")) {
        // Provavelmenteo o token foi expirado. Então logout
        // this.$store.dispatch("newAlert", {
        //   type: "alert",
        //   text: "Your session was expired"
        // });
        console.log("axios plugin error: ");
        console.log(error.response.mesage);
        // this.$store.dispatch("logout");
      }
    }

    return Promise.reject(error);
  }
);

Plugin.install = function(Vue, options) {
  Vue.axios = options;
  window.$axios = options;
  Object.defineProperties(Vue.prototype, {
    axios: {
      get() {
        return options;
      }
    },
    $axios: {
      get() {
        return options;
      }
    }
  });
};

Vue.use(Plugin, _axios);

export default _axios;
