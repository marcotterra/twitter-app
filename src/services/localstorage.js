/** Classe munipuladora do localStorage */
const localStorage = {
  /**
   * Função que retorna um terminado dado no localStorage
   * @param {String} key Identificador único do dado
   * @returns {String} Retorna o conteúdo dado requisitado
   */
  getToken: key => window.localStorage.getItem(key),

  /**
   * Função que insere um terminado dado no localStorage
   * @param {String} key Identificador único do dado
   * @param {String} value Valor do dado a ser salvo
   */
  setToken: (key, value) => window.localStorage.setItem(key, value),

  /**
   * Função que remove um terminado dado no localStorage
   * @param {String} key Identificador único do dado
   */
  removeToken: key => window.localStorage.removeItem(key)
};

export default localStorage;
