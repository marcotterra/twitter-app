import Vue from "vue";
import RouterSync from "vuex-router-sync";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "@babel/polyfill";
import "./plugins/axios";
import "./plugins/vuetify";
import "./plugins/moment";
import "material-design-icons-iconfont/dist/material-design-icons.css";

Vue.config.productionTip = false;

RouterSync.sync(store, router);

new Vue({
  el: "#app",
  router,
  store,
  render: h => h(App)
});
