import store from "@/store";
import app from "@/configs/app";

const changePageTitle = title => {
  store.dispatch("setTitle", title);
  return (window.document.title = `${app.appName} - ${title}`);
};

const logaIsso = val => {
  console.log(val);
};

export { changePageTitle, logaIsso };
