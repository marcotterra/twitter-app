import Vue from "vue";
import Router from "vue-router";

import store from "@/store";
import { changePageTitle } from "@/utils/functions";

Vue.use(Router);

const routes = [
  {
    path: "/index.html",
    redirect: { name: "dashboard" }
  },
  {
    path: "/",
    name: "dashboard",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "dashboard" */ "@/views/Dashboard"),
    meta: {
      title: "Home",
      requiresAuth: true
    }
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ "@/views/About"),
    meta: {
      title: "test",
      requiresAuth: true
    }
  },
  {
    path: "/login",
    name: "loginpage",
    component: () =>
      import(/* webpackChunkName: "loginpage" */ "@/views/Login"),
    meta: {
      title: "Login page",
      requiresAuth: false
    }
  },
  {
    path: "/logout",
    name: "logoutpage",
    component: () =>
      import(/* webpackChunkName: "logoutpage" */ "@/views/Logout"),
    meta: {
      title: "Logout page",
      requiresAuth: true
    }
  },
  {
    path: "/register",
    name: "registerpage",
    component: () =>
      import(/* webpackChunkName: "registerpage" */ "@/views/Register"),
    meta: {
      title: "Register page",
      requiresAuth: false
    }
  }
];

const router = new Router({
  mode: "history",
  routes,
  base: process.env.BASE_URL
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.title)) {
    changePageTitle(to.meta.title);
  }

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.isAuth) {
      next({
        path: "/login",
        query: { redirect: to.fullPath }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
